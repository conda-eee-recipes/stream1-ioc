#!/bin/bash

mkdir -p ${CONDA_PREFIX}/startup/ioc
cp st.cmd ${CONDA_PREFIX}/startup/ioc/

cat <<EOF > ${CONDA_PREFIX}/bin/run_ioc
#!/bin/bash

iocsh ${CONDA_PREFIX}/startup/ioc/st.cmd
EOF
chmod a+x ${CONDA_PREFIX}/bin/run_ioc

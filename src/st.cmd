require stream1,1.0.0
require recsync,1.2.0

#Specify the TCP endpoint and give your 'bus' an arbitrary name eg. "asynstream1".
drvAsynIPPortConfigure("asynstream1", "localhost:4001")

#Load your database defining the EPICS records
dbLoadRecords(stream1.db)
